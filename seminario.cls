\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{seminario}
\RequirePackage{ifthen}
\RequirePackage[utf8]{inputenc}

\DeclareOption{oneside}{\newcommand{\seminario@side}{oneside}}
\DeclareOption{twoside}{\newcommand{\seminario@side}{twoside}}

\DeclareOption{portugues} {
    \newcommand{\seminario@lang}{portuges}
    \newcommand{\seminario@lecturename}{Aula}
    \newcommand{\seminario@theoremname}{Teorema}
    \newcommand{\seminario@propositionname}{Proposição}
    \newcommand{\seminario@lemmaname}{Lema}
    \newcommand{\seminario@corollaryname}{Corolário}
    \newcommand{\seminario@definitionname}{Definição}
    \newcommand{\seminario@examplename}{Exempplo}
    \newcommand{\seminario@exercisename}{Exercício}
    \newcommand{\seminario@remarkname}{Observação}
    \newcommand{\seminario@factname}{Fato}
    \newcommand{\seminario@problemname}{Problema}
    \newcommand{\seminario@conjecturename}{Conjectura}
    \newcommand{\seminario@claimname}{Afirmação}
    \newcommand{\seminario@notationname}{Notação}
}

\ProcessOptions
%%% Load default arguments if empty
\ifthenelse{\isundefined{\seminario@side}}{\ExecuteOptions{oneside}}{}
\ifthenelse{\isundefined{\seminario@lang}}{\ExecuteOptions{portugues}}{}

%%% Load default class
\LoadClass[a4paper,12pt,\seminario@side]{article}


%%% Common macroes
\RequirePackage[\seminario@lang]{babel}
\RequirePackage[pdftex,colorlinks]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage[nodayofweek]{datetime}
\RequirePackage[left=3cm,right=2cm,top=2.5cm,bottom=2cm]{geometry}
\RequirePackage{enumerate}
\RequirePackage{tikz}
\RequirePackage{tkz-euclide}
\RequirePackage{bbm}
\usetikzlibrary{arrows}
\usetikzlibrary{patterns}
\usetikzlibrary{calc}
%\usetikzlibrary{decoration}
\usetikzlibrary{snakes}
\usetikzlibrary{shapes}
\RequirePackage[intlimits,leqno, fleqn]{amsmath}
\RequirePackage[osf,sc]{mathpazo}
\linespread{1.05} % Palatino
\RequirePackage{amsfonts}
\RequirePackage{amsthm}
\RequirePackage{amsxtra}
\RequirePackage{amssymb}
\RequirePackage{mathdots}
\RequirePackage{mathrsfs}
\RequirePackage{stmaryrd}
\RequirePackage{titlesec}
\newcommand{\contentsfinish}{}
\RequirePackage[titles]{tocloft}
\RequirePackage{textcase}
\RequirePackage{setspace}
\RequirePackage{xfrac}
\RequirePackage{mathtools}
\RequirePackage{cancel}
\RequirePackage{mparhack}
\RequirePackage{booktabs}
\RequirePackage{multirow}
\RequirePackage[fixlanguage]{babelbib}

\delimitershortfall=5pt

\frenchspacing
\DeclareRobustCommand{\smallcaps}[1]{\scshape{#1}}%\MakeTextLowercase{#1}}
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
    \hbox{}
    \vspace*{\fill}
    \vspace{\fill}
    \thispagestyle{empty}
    \newpage
    \if@twocolumn\hbox{}\newpage\fi\fi\fi}
\newcommand{\mymarginpar}[1]{\marginpar{\raggedright{} \setstretch{0.7}\textit{\scriptsize{#1}}}}
\def\hrulefill{\leavevmode\leaders\hrule height 2pt\hfill\kern\z@}
\newcommand*\ruleline[1]{\par\noindent\raisebox{.6ex}{\makebox[\linewidth]{\hrulefill\hspace{1ex}\raisebox{-.6ex}{#1}\hspace{1ex}\hrulefill}}} 
\newcommand{\separator}{\begin{center}\rule{\columnwidth}{\arrayrulewidth}\end{center}}
\newcommand{\tosay}[1]{\begin{center}\text{\fbox{\scriptsize{#1}}}\end{center}}
\newcommand{\TODO}{\mymarginpar{TODO}}%

%%% Theorems and sections style
\newtheoremstyle{plainsc}{10pt}{10pt}{\sl}{}{\bfseries}{.}{.5em}{}
\newtheoremstyle{definitionsc}{10pt}{10pt}{\normalfont}{}{\bfseries}{.}{.5em}{}
\newtheoremstyle{remarksc}{10pt}{10pt}{\normalfont}{}{\bfseries}{.}{.5em}{}

\theoremstyle{plainsc}
\newtheorem{theorem}{\seminario@theoremname}[section]
\newtheorem{proposition}[theorem]{\seminario@propositionname}
\newtheorem{lemma}[theorem]{\seminario@lemmaname}
\newtheorem{corollary}[theorem]{\seminario@corollaryname}
\newtheorem{conjecture}[theorem]{\seminario@conjecturename}
\newtheorem{claim}[theorem]{\seminario@claimname}
\theoremstyle{definitionsc}
\newtheorem{definition}[theorem]{\seminario@definitionname}
\theoremstyle{remarksc}
\newtheorem{example}[theorem]{\seminario@examplename}
\newtheorem{exercise}[theorem]{\seminario@exercisename}
\newtheorem{remark}[theorem]{\seminario@remarkname}
\newtheorem{fact}[theorem]{\seminario@factname}
\newtheorem{problem}[theorem]{\seminario@problemname}
\newtheorem{notation}[theorem]{\seminario@notationname}

%%% Preamble commands (as title, author, ...)
\newcommand{\nomeseminario}[1]{\newcommand{\seminario@nome}{#1}}
\newcommand{\semestre}[1]{\newcommand{\seminario@semestre}{#1}}
\newcommand{\ano}[1]{\newcommand{\seminario@ano}{#1}}
\newcommand{\site}[1]{\newcommand{\seminario@site}{#1}}

\newcounter{seminario@numero}
\newcommand{\seminario@palestrante}{Yoshiharu Kohayakawa}
\newcommand{\seminario@mostradata}{ }
\def\aula do dia #1/#2 sobre #3 por #4.{%
    \stepcounter{seminario@numero}%
    \renewcommand{\seminario@palestrante}{#4}
    \def\seminario@assunto{#3}
    \renewcommand{\seminario@mostradata}{#1 de \monthname[#2]}
    
    {\sl\noindent $\diamond$ \hfil $\diamond$ \hfil $\diamond$ \hfill
         Aula \theseminario@numero (\seminario@mostradata) --- \seminario@palestrante 
         \hfill $\diamond$ \hfil $\diamond$ \hfil $\diamond$\par}
}



\def\RR{\mathcal{R}}

\DeclareMathOperator{\conv}{conv}
\def\cA{\ensuremath{\mathcal{A}}}
\def\cU{\ensuremath{\mathcal{U}}}
\def\CC{\ensuremath{\mathbb{C}}}
\def\cC{\ensuremath{\mathcal{C}}}
\def\EE{\ensuremath{\mathbb{E}}}
\def\FF{\ensuremath{\mathbb{F}}}
\def\cH{\ensuremath{\mathcal{H}}}
\def\KK{\ensuremath{\mathbb{K}}}
\def\cL{\ensuremath{\mathcal{L}}}
\def\NN{\ensuremath{\mathbb{N}}}
\def\PP{\ensuremath{\mathbb{P}}}
\def\RR{\ensuremath{\mathbb{R}}}
\def\cS{\ensuremath{\mathcal{S}}}
\def\TT{\ensuremath{\mathbb{T}}}
\def\cT{\ensuremath{\mathcal{T}}}
\def\ZZ{\ensuremath{\mathbb{Z}}}
\def\cF{\ensuremath{\mathcal{F}}}
\def\cG{\ensuremath{\mathcal{G}}}
\def\cP{\ensuremath{\mathcal{P}}}
\def\One{\ensuremath{\mathbbm{1}}}

\def\uu{\ensuremath{\mathbf{u}}}
%\newcommand{\demex}{(dem. - exercício!)}
\def\eps{\varepsilon}
%\DeclareMathOperator{\lim}{lim}
% \DeclareMathOperator{\interior}{int}
% \DeclareMathOperator{\pint}{int}
% \DeclareMathOperator{\pfrac}{frac}
% \DeclareMathOperator{\mdc}{mdc}
% \DeclareMathOperator{\mood}{mod}
% \DeclareMathOperator{\gr}{gr}
% \DeclareMathOperator{\id}{id}
% \DeclareMathOperator{\Imag}{Im}
% \newcommand{\Mod}[1]{\ (\mood\ #1)}

%------ Scalable delimiters -------
\def\<{\left\langle}
\def\>{\right\rangle}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
%\def\lv{\left\lvert}
%\def\mv{\middle\vert}
%\def\rv{\right\rvert}
%\def\lV{\left\lVert}
%\def\mV{\middle\Vert}
%\def\rV{\right\rVert}
%\def\llb{\left\llbracket}
%\def\rrb{\right\rrbracket}
%--------------------------------------



\AtBeginDocument{
  \shortdate
  \pagestyle{fancy}
  \fancyhead[L]{\footnotesize \smallcaps \seminario@assunto\  (\seminario@palestrante)}
  \fancyhead[R]{\footnotesize \smallcaps  \seminario@mostradata}
  \fancyfoot[C]{\thepage}
%  \renewcommand{\headrulewidth}{0pt}
  \reversemarginpar
  \setlength{\marginparwidth}{2cm}
\usetkzobj{all}
  \maketitle
  \tableofcontents
}

%%% Common structures

\renewcommand\maketitle{\par
  \begingroup
  \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
  \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
  \long\def\@makefntext##1{\parindent 1em\noindent
    \hb@xt@1.8em{%
      \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
  \@maketitle
  \thispagestyle{plain}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
}

\renewcommand{\@maketitle}{
  \newpage
  \null
  \vskip 4ex%
  \begin{center}%
    \let \footnote \thanks
    {\small\smallcaps notas de aula do\par}
    \vskip10pt
    {\Large\smallcaps \seminario@nome}%
    {{\vskip 1ex\large\smallcaps Programa de Iniciação Científica e Mestrado\\em\\Combinatória}}%
    \vskip 1.5em%
    {\large
    \begin{tabular}[t]{c}%
        {\small\url{\seminario@site}}%
    \end{tabular}\par}%
    \vskip 2ex%
    {\small\itshape Anotado por: \@author}%
    \vskip 1ex%
    {\small\itshape{\seminario@semestre\textsuperscript{o} semestre de \seminario@ano}}%
  \end{center}%
  \par
  \vskip 3ex
}


