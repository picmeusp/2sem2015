\subsection{Ultrafiltros}
Nesta seção, $X$ será um conjunto, geralmente infinito, 
como o conjunto do naturais $\NN = \{1,2,\dots\}$. Se 
$A$ é um subconjunto de $X$, denotaremos $X\setminus A$ por $A^c$.

 Uma coleção 
$\cF\subseteq 2^X$ é um \emph{filtro} sobre $X$ se 
\begin{enumerate}
    \item $\emptyset \notin \cF, X\in \cF$;
    \item $\cF$ é fechado por superconjuntos, isto é, 
    se $A\subseteq B$ e $A\in\cF$, então $B\in\cF$;
    \item $\cF$ é fechado por intersecções, isto é, 
    se $A,B\in \cF$, então $A\cap B\in \cF$.
\end{enumerate}

Dizemos que um filtro $\cF$ é um \emph{ultrafiltro} se 
a seguinte condição extra for satisfeita:
\begin{enumerate}
    \item[4] \label{cond:ultraf} 
    Para todo $A\subseteq X$, $A$ ou $A^c$ pertence a $\cF$.
\end{enumerate}

\begin{remark}
    Ultrafiltros $\cU$ podem ser pensados como uma classificação dos 
    subconjuntos de $X$ emm duas categorias: conjuntos \emph{grandes}
    (membros de $\cU$) e conjuntos \emph{pequenos} (fora de $\cU$).
\end{remark}

Ultrafiltros podem também ser definidos como uma medida aditivia 
tomando apenas os valores $0$ ou $1$. Isto é, se $\cU$ é um ultrafiltro, 
então podemos definir a medida 
\begin{align*}
    m_{\cU}: 2^X &\to \{0,1\}\\
              A &\mapsto \begin{cases}
                        0, \text{ se $A\in\cU$;}\\
                        1, \text{ se $A\notin\cU$.}
                    \end{cases}
\end{align*}
A medida $m_{\cU}$ é aditiva, isto é, se 
$A,B\subset X$ e $A\cap B = \emptyset$, então
$$m_{\cU}(A\cup B) = m_{\cU}(A) + m_{\cU}(B).$$
Para verificar essa igualdade, notamos que a condição \ref{cond:ultraf}
da definição de ultrafiltros pode ser trocada pela seguinte:
\begin{enumerate}
    \item[4'] Se $C\in\cF$ e $C = A\cup B$, então $A\in \cF$ ou $B\in\cF$.
\end{enumerate}
De fato, se $A\notin \cF$ e $B\notin\cF$, então a condição \ref{cond:ultraf}
implica que $A^c\in\cF$ e $B^c\in\cF$, da onde segue que $C^c = A^c\cap
B^c\in\cF$, e, novamente pela condição \ref{cond:ultraf}, que $C\notin \cF$.

As seguintes famílias são exemplos de filtros.
\begin{itemize}
    \item $\cF = \{X\}$ (filtro trivial).
    \item $\emptyset \not= Y\subset X$, $\cF_Y = \{A\subseteq X: Y\subseteq A\}.$
    \item $\cF_{cofin} = \{A\subseteq X: A^c \text{ é finito}\}$ (filtro de Frechet).
\end{itemize}

Dado $x\in X$, a família 
$$\cF_x = \cF_{\{x\}} = \{A\subseteq X: x\in A\}$$
é um exemplo de ultrafiltro. Ultrafiltros dessa forma, são chamados 
de \emph{ultrafiltros principais}.

Dizemos que um filtro $\cF$ é \emph{maximal} se
$$\cF\subseteq \cF' \text{ e } \cF'\text{ é filtro } \Rightarrow \cF = \cF'.$$

\begin{fact}\label{fac:ultramax}
    Seja $\cF\subseteq 2^X$ um filtro. Então 
    $\cF$ é um ultrafiltro se, e somente se, $\cF$ é um filtro maximal.
\end{fact}
\begin{proof}
    Se $\cF$ é ultrafiltro e $A\notin \cF$, então $A^c\in \cF$ e, portanto, 
    $\cF\cup A$ não pode ser ul ultrafiltro.

    Para provar a recíproca, suponha que $\cF$ tal que 
    $A,A^c\notin \cF$ para algum $A\subseteq \cF$. Então condidere 
    o filtro $\cF'$ gerado por $\cF\cup \{A\}$, isto é, formado 
    por conjuntos em $\cF$ e por $A$ e fechado por interseção 
    finita e por superconjuntos. 
\end{proof}

\begin{remark}
    Se $|X|<\infty$, então os ultrafiltros sobre $X$ são todos da forma
    $\cF_x$.
\end{remark}

\begin{remark}
    Suponha $X$ infinito e $\cU$ um ultrafiltro sobre $X$ \emph{não-principal}, 
    isto é, $\cU \not= \cF_x$, para todo $x\in X$. Então $U\supseteq \cF_{cofin}$.
\end{remark}

\begin{theorem}
    Seja $X$ um conjunto, com $|X| = \infty$. Então existem ultrafiltros 
    não principais sobre $X$
\end{theorem}
\begin{proof}
    Seja $\FF = \{\text{filtros }\cF\subseteq 2^X: \cF\supseteq \cF_{cofin}\}$.
    Note que $\FF$ é parcialmente ordenado por inclusão.  Usamos o Lema de Zorn
    para provar que $\FF$ contém um elemento maximal $\cF^*$. Tal $\cF^*$ é um
    ultrafiltro (Fato \ref{fac:ultramax}).

    Fixe uma cadeia arbitrária $(\cF_\lambda)_{\lambda\in\Lambda}$ em $\FF$, isto é, um conjunto 
    totalmente ordenado (por inclusão). Para que possamos aplicar 
    o Lema de Zorn, precisamos mostrar que existe um $F_0\in\FF$ tal que 
    para todo $\lambda$, $\cF_{\lambda}\subseteq F_0$. Basta tomar
    $\cF_0 = \bigcup_{\lambda\in\Lambda} \cF_{\lambda}$
    e notar que $\cF_0$ é um filtro e contém $\cF_{cofin}$.

    Logo, pelo Lema de Zorn, $\FF$ tem um elemento maximal $\cF^*$. Tal 
    $F^*$ é um ultrafiltro (Fato \ref{fac:ultramax}) não-principal (pois 
    contém $\cF_{cofin}$).
\end{proof}

\subsection{Ultrafiltros e topologia}
Seja $a_1,a_2,\dots \in [0,1]$. Dizemos que 
$\lim a_n = L$ se para todo $\eps > 0$, existe inteiro $n_0$ tal que 
para todo $n > n_0$, $a_n\in (L-\eps, L+\eps)$.

Seja $A_\eps = \{n: n\geq n_0\}$. Então $A_\eps \in \cF_{cofin}$, onde
$\cF_{cofin}$ é o filtro dos elementos cofinitos sobre $\NN$.  Filtros podem
ser usados para generalizar o conceito de limites de sequências. No que se
segue, sequências em um conjunto $Y$ serão representadas como funções $f:\NN\to
Y$ (isto é $a_n = f(n)$).

\begin{fact}
Seja $Y$ um conjunto e $f:\NN\to Y$. Suponha que 
$\cF$ seja um filtro/ultrafiltro sobre $\NN$. Defina
$$f*(\cF) = \{A\subseteq Y: f^{-1}(A)\in \cF\}.$$
Então $f^*(\cF)$ é um filtro/ultrafiltro sobre $Y$.
\end{fact}

Seja $\cF$ um filtro sobre um espaço topológico $Y$.  Dizemos que $\cF$
\emph{converge} a um ponto $y\in Y$ se todo aberto $\cU$ em $Y$ com $y\in \cU$
é um membro de $\cF$.

Seja $Y$ um espaço topológico, $f:\NN\to Y$ e $\cF$ um filtro sobre
$\NN$. Então $y\in Y$ é um \emph{$\cF$-limite} de $f$ se~$f_*(\cF)$
converge para~$y$.

