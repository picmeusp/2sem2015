\subsection{Prova de Bárány ('78) para Conjectura de Kneser}
A demonstração do Teorema \ref{th:kneserlovasz} (Conjectura de Kneser) 
da aula passada depende do Teorema \ref{th:lyusternikMisto} sobre topologia. 
Veremos nesta aula uma outra demonstração para o Teorema \ref{th:kneserlovasz} que 
depende apenas do seguinte resultado de topologia, que é equivalente ao 
Teorema de Bursuk-Ulam.

\begin{theorem}[Lyusternik-Schnirelmann]\label{th:lyusternik}
    Se a esfera $S^d\in\RR^d$ é coberta por conjuntos $C_1,\dots,C_{d+1}$ \emph{abertos}, isto é, 
    se $$S^d = C_1\cup \dots \cup C_{d+1},$$
    então existe $i$ e $x\in S^d$ tais que $x,-x\in C_i$. 
\end{theorem}


Consideraremos hiperplanos em $\RR^d$ que passam pela origem. 
Mais especificamente, um hiperplano $h\in \RR^d$ e os semiespaços 
abertos correspondetes $h^+$ e $h^-$ são determinados por um 
um vetor $a\in \RR^d$ da seguinte maneira:
\begin{align*}
    h &= \{x\in\RR^d: \<a,x\> = 0\}, \\
    h^+ &= \{x\in \RR^d: \<a,x\> > 0\}, \\
    h^- &= \{x\in\RR^d: \<a,x\> < 0\}.
\end{align*}


\begin{lemma}[Lema de Jade ('56)]\label{lemma:jade}
    Para quaisquer inteiros~$d\geq 0$ e~$k\geq 1$, existe~$X\subset S^d\subset
    \RR^{d+1}$, com~$|X|=2k+d$, tal que para todo hiperplano $h$ (passando 
    pela origem) os semiespaços abertos~$h^{+}$ e~$h^{-}$ são tais 
    que:
    $$|h^+ \cap X|\geq k \qquad\text{e}\qquad |h^- \cap X|\geq k.$$
\end{lemma}
\begin{proof}
    Vamos construir~$V=\{v_1,\dots,v_{2k+d}\}\subset \RR^{d+1}$ tal que
    para todo hiperplano~$h$, $|h^+\cap V|,|h^-\cap V|\geq k$. Assim, 
    podemos tomar $X$ como abaixo.
    $$X = \left\{\frac{v_1}{\|v_1\|}, \dots, \frac{v_{2k+d}}{\|v_{k+d}\|}\right\}$$
    Consideramos a curva dos momentos 
    $$\overline{\gamma}(t)=(1,t,t^2,\dots,t^d)\in \RR^{d+1} \quad (t\in\RR).$$
    Seja~$W=\{w_1,\dots,w_{2k+d}\}$ um conjunto de pontos sobre~$\overline\gamma$ ``em ordem''. Por
    exemplo, podemos tomar~$w_i = \overline\gamma(i)$~$(1\leq i\leq 2k+d)$.
    
    Pomos $v_i=(-1)^i w_i$.
    Mostraremos a seguir que $|h^+\cap V|,|h^-\cap V|\geq k$, ou, 
    equivalentemente, que:
    \begin{enumerate}
        \item[{\color{blue} 1.}]
            $|\{w_i\in h^{+}: i \text{ é {par}}\}| + 
            |\{w_i\in h^{-}: i \text{ é {ímpar}}\}| \geq k$ \text{ e }
        \item[{\color{red} 2.}]
            $|\{w_i\in h^{-}: i \text{ é {par}}\}| + 
            |\{w_i\in h^{+}: i \text{ é {ímpar}}\}| \geq k.$
    \end{enumerate}
    Seja~$W_{on} = W\cap h$ o conjunto dos $w_i\in W$ que pertencem ao 
    hiperplano $h$. Note que $W_{on}\leq d$. De fato, suponha que 
    $h$ seja dado pelos pontos ortogonais a $a=(a_1,\dots,a_{d+1})\in \RR^d$.
    Então todo $t\in\RR$ tal que $\overline\gamma(t)\in h$ satisfaz a equação
    $$a_1+a_2t+a_3t^2+\dots+a_{d+1}t^d = \<a,\overline\gamma(t)\>=0,$$
    que admite no máximo $d$ raízes reais.
    
    Podemos supor, sem perda de generalidade, que $W_{on}=d$, movendo o
    hiperplano $h$ até que exatamente $d$ pontos de $W$ pertençam a $h$, e de
    forma a não mudar nenhum ponto de lado. 

    Considere o conjunto $W_{off} = W\setminus W_{on}$. Colorimos
    cada ponto $w_i$ em $W_{off}$ de 
    \begin{itemize}
        \item {\color{blue} azul}, se $i$ for par e pertencer a $h^+$, ou 
                                   se $i$ for ímpar e pertencer a $h^-$.
        \item {\color{red} vermelho}, se $i$ for par e pertencer a $h^-$, ou 
                                   se $i$ for ímpar e pertencer a $h^+$.
    \end{itemize}
    {\hfill \begin{tikzpicture}[scale=.5]%,cap=round,>=latex]
        \filldraw[
            draw=gray,%
            fill=gray!20,%
        ]   (0,0)--(7,5)--(27,5)--(20,0)--cycle;
        \draw[thick,->] (0,0)-- (0,3)  node[midway,above,right] {$a$};
        \draw[thick] plot[domain=0:pi] ((1.2*\x+7,{3.3*sin(\x r)+2.7});
        \draw[thick,dotted] plot[domain=pi:pi*2] ((1.2*\x+7,{3.3*sin(\x r)+2.7});
        \draw[thick] plot[domain=pi*1.3:pi*1.7] ((1.2*\x+7,{3.3*sin(\x r)+2.7});
        \draw[thick] plot[domain=pi*2:pi*3] ((1.2*\x+7,{3.3*sin(\x r)+2.7});
        \draw[thick,dotted] plot[domain=pi*3:pi*3.3] ((1.2*\x+7,{3.3*sin(\x r)+2.7});
        \draw[thick] plot[domain=pi*3.3:pi*4.2] ((1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{0}
         \coordinate [label=above:\scriptsize $w_1$] (W1) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*.2}
         \coordinate [label=above:\scriptsize $w_2$] (W2) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*.5}
         \coordinate [label=above:\scriptsize $w_3$] (W3) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*.7}
         \coordinate [label=above:\scriptsize $w_4$] (W4) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*1}
         \coordinate [label=above:\scriptsize $w_5$] (W5) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*1.2}
         \coordinate [label=above:\scriptsize $w_6$] (W6) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*1.8}
         \coordinate [label=above:\scriptsize $w_7$] (W7) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*2}
         \coordinate [label=above:\scriptsize $w_8$] (W8) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*2.2}
         \coordinate [label=above:\scriptsize $w_9$] (W9) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*2.7}
         \coordinate [label=above:\scriptsize $w_{10}$] (W10) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*3}
         \coordinate [label=above:\scriptsize $w_{11}$] (W11) at (1.2*\x+7,{3.3*sin(\x r)+2.7});
        \def\x{pi*3.2}
         \coordinate [label=above:\scriptsize $w_{12}$] (W12) at (1.2*\x+7,{3.3*sin(\x r)+2.7});

        \tkzDrawPoints[color=black](W1,W5,W8,W11);
        \tkzDrawPoints[color=blue](W2,W4,W7,W10);
        \tkzDrawPoints[color=red](W3,W6,W9,W12);
    \end{tikzpicture}\hfill}

    Note que ao longo de $\overline{\gamma}$ os pontos em $W_{off}$ estão 
    coloridos alternadamente de azul e vermelho. Como $|W_{off}| = 2k$, 
    concluímos que exatamente $k$ pontos em $W_{off}$ serão coloridos de azul
    e exatamente $k$, de vermelho. 
\end{proof}
\begin{remark}
    Não poderíamos trocar a hipótese de que $|X|=2k+d$ por $|X|=2k+d-1$ no
    enunciado do Lema \ref{lemma:jade}. Nesse caso, ao tomarmos um hiperplano $h$ passando 
    por quaisquer $d$ pontos de $X$, restariam apenas $2k-1$ pontos nos 
    espaços abertos $h^+$ e $h^-$ (e portanto algum deles teria $k-1$
    pontos).
\end{remark}

\begin{proof}[Demonstração de Bárány para a Conjectura de \ref{th:kneserlovasz}]
    Seja $d = n-2k$ e $X\subset S^d\subset \RR^{d+1}$ um conjunto de $n$ pontos 
    em $S^d$ como no Lema de Gale. Identificamos $[n]$ com $X$. 

    Suponha por contradição que 
    $$N = \binom{[n]}{k} = \cC_1 \cup \dots \cup \cC_{n-2k+1}$$
    é tal que não existe $A,B\in \cC_i$ com $A\cap B\not=\emptyset$, isto é, que 
    todos os $\cC_i$ sejam intersectantes.

    Para todo $i\in\{0,\dots,n-2k+1\}$, pomos
    $$A_i = \left\{x\in S^d: H(x) \text{ contém um $S\in \binom{X}k$ com $S\in \cC_i$}\right\}, $$
    onde $H(x) = \{y\in \RR^d:\<x,y\> > 0\}$.
    Esses $A_i$ são abertos (\emph{exercício}). Ademais, segue da escolha 
    de $X$ e do Lema de Gale que tais $A_i$ cobrem $S^d$. 

    Logo, pelo Teorema \ref{th:lyusternik}, existe $i$ tal que $x,-x\in A_i$. Isso 
    implica a existência de conjuntos $S,S'\in \binom{X}k$ coloridos, ambos, com a
    cor $i$ mas contidos, respectivamente, em $H(x)$ e $H(-x)$ e, portanto, disjuntos.
\end{proof}

\subsection{Teorema de Schrijver}

Seja $C_n$ o circuito de comprimento $n$, cujos vértices (na ordem em que aparecem no 
circuito) são os inteiros $1,\dots,n$. Um conjunto de vértices $S\subset [n]$ de $C_n$
é \emph{estável em $C_n$} se não induz uma aresta. 

Seja 
$${\binom{[n]}k}_{\kern-5pt stab} = \left\{S\in\binom{[n]}k : \text{$S$ é estável em $C_n$}\right\}.$$
O seguinte resultado afirma que o Conjectura de Kneser vale mesmo quando 
particionamos ${\binom{[n]}k}_{\kern-3pt stab}\subset \binom{[n]}k$.

\begin{theorem}[Schrijver]\label{th:schrijver}
    Seja $n>2k-1$ e suponha que existam conjuntos
      $\cC_1, \dots, \cC_{n-2k+1} \in 2^{\binom{[n]}{k}}$ tais que 
    $${\binom{[n]}{k}}_{\kern-5pt stab} = \cC_1 \mathbin{\dot\cup} \dots \mathbin{\dot\cup} \cC_{n-2k+1}.$$
    Então existe $i\in[n-2k+1]$ e conjuntos $A,B\in \cC_i$ tais que $A\cap B=\emptyset$.
\end{theorem}

