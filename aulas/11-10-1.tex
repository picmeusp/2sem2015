\begin{corollary}
    Um inteiro $n\geq 0$ é representável se, e somente se, todo primo da forma
    $4m+3$ aparece com expoente par na decomposição de $n$.
\end{corollary}
\begin{proof}
    Usaremos os seguintes fatos:
    \begin{enumerate}[i)] 
        \item $1$ e $2$ são representáveis.
        \item Se $n$ é representável, então $z^2n$ é representável para qualquer
        inteiro $n$. De fato, se $n = a^2 + b^2$, então $z^2n = (za)^2 +
        (zb)^2$.
        \item Se $x$ e $y$ são representáveis, então $xy$ é representável.
        De fato, suponha que $x = a^2 + b^2$ e $y = c^2 + d^2$. Então 
        \begin{align*}
            xy &= a^2c^2 + a^2d^2 + b^2c^2 + b^2d^2\\
               &= a^2c^2 - 2abcd + b^2d^2 + a^2d^2 + 2abcd + b^2c^2 \\
               &= (ad - bc)^2 + (ac + bd)^2.
        \end{align*}
    \end{enumerate}
    Seja $n$ um inteiro tal que todo primo da forma $4m+3$ aparece com expoente 
    par na decomposição de $n$. Então, como os demais fatores primos são
    representáveis pelo Teorema~\ref{th:fermatsq}, segue dos fatos acima que 
    $n$ é representável. 

    Por outro lado, seja $p$ um primo da forma $p = 4m+3$, tal que 
    $p|n$ e  $n = x^2 + y^2$. Afirmamos que $p|x$ e $p|y$. De fato, temos 
    $x^2 + y^2\equiv 0 (mod p)$. Mas se $x\not\equiv 0 (mod p)$, podemos multiplicar 
    ambos os lados da equação anterior por $x^{-2}$ para obter que 
    $(x^{-1}y)^2 \equiv -1 (mod p)$, que não tem soluções quando $p = 4m+3$.
    %TODO!!
    Então, $p\mid x$ e, analogamente, $p\mid y$, o que implica $p^2 | n$.
    Logo, $\frac{n}{p^2} = \left(\frac{x}{p}\right)^2  + \left(\frac{y}{p}\right)^2$ 
    também é representável. Segue, por indução, que $p$ aparece com 
    expoente par em $\frac{n}{p^2}$ e, portanto, também em~$n$.
\end{proof}

Seja $A$ um conjunto. Uma função $f:A\to A$  é uma \emph{involução} se 
$f = f^{-1}$. 

\begin{fact}
    Seja $A$ finito e $f:A\to A$ uma involução. Então 
    $$|A|\equiv |\{x: f(x)=x\}| (mod 2),$$
    isto é, 
    $|A|$ tem a mesma paridade que o número de pontos 
    fixos de $f$.
\end{fact}
\begin{proof}
    Para $x,y\in A$, dizemos que $x\sim y \Leftrightarrow x = f(y)$.
    Particione $A$ de acordo com as classes definidas por 
    $\sim$\footnote{TODO:não é bem uma classe de equivalência}
    Como $f$ é uma involução, cada classe é composta por um ou por dois 
    elementos. As classes de tamanho $1$ são, exatamente, os pontos fixos 
    de $f$.
\end{proof}

\begin{proof}[Prova do Teorema~\ref{th:fermatsq}]
    Suponha que $p\equiv 1 (mod 4)$ seja um primo. Definimos
    os seguintes três conjuntos de triplas de inteiros:
    \begin{align*}
        S &= \{(x,y,z)\in\ZZ^3: 4xy + z^2 = p, x>0, y>0\},\\
        T &= \{(x,y,z)\in S: z>0\} \text{ e }\\
        U &= \{(x,y,z)\in S: x-y+z > 0\}.
    \end{align*}
    Conside a função 
    \begin{align*}
        f:S&\to S\\
        (x,y,z)\mapsto (y,x,-z).
    \end{align*}
    Primeiro note que $f$ é uma involução e está bem definida pois $4xy+z^2 =
    4(yx)+(-z)^2$.  Ademais, $f$ não tem pontos fixos, caso contrário teríamos
    $z = 0$ o que implicaria $p\equiv 0 (mod 4)$. 
    Note também que
    \begin{enumerate}[i)]
        \item Se $(x,y,z)\in T$, então $f(x,y,z)\in S\setminus T$.
        Analogamente, se $(x,y,z)\in S\setminus T$, então $f(x,y,z)\in T$ 
        (lembrando que $z\not= 0$).
        \item Se $(x,y,z)\in U$, então $f(x,y,z)\in S\setminus U$, pois 
        se $x-y+z = z-(y-x)>0$, então $(y-x)-z < 0$. Analogamente, 
        se $(x,y,z)\in S\setminus U$, então $f(x,y,z)\in U$ (note que 
        $x-y+z\not= 0$, caso contrário teríamos 
        $p = (y-x)^2 + 4xy = (y+x)^2$).
    \end{enumerate}
    Concluímos, então, que $f(U\setminus T) = f(T\setminus U)$. Segue que 
    $|U\setminus T| = |T\setminus U|$, e portanto, que $|U|=|T|$.

    Considere agora a função 
    \begin{align*}
        g:U&\to U\\
        (x,y,z)&\mapsto (x-y+z,y,2y-z)
    \end{align*}
    Observe que $g$ está bem definida pois 
    $4(x-y+z)y + (2y-z)^2 = 4xy + z^2$ e 
    $(x-y+z) - y + (2y-z) = x > 0$. 
   
    Além disso,  
    $(x,y,z)$ é um ponto fixo de $g$ se, e somente se, 
    $y=z$. Mas neste caso, devemos ter 
    $y(4x+y) = 4xy+y^2 = p$, o que implica que $y=1$ e 
    $4x+1=p$. Logo $(\frac{p-1}4,1,1)$ é o \emph{único}
    ponto fixo de $g$. Concluímos que $|U|$ é ímpar (e, portanto, 
    $|T|$ é ímpar).

    Finalmente, considere a função 
    \begin{align*}
        h:T&\to T\\
     (x,y,z)&\mapsto (y,x,z)
    \end{align*} 
    Note que $h$ está bem definido e é uma involução. Mas como 
    $|T|$ é ímpar, $h$ tem pelo menos um ponto fixo. Existem portanto
    $x,z\in \ZZ$ tais que $4x^2 + z^2 = p$, da onde segue que 
    $p$ é representável.
\end{proof}





    

    
        
