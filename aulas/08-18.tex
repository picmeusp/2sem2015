A seguir, veremos dois problemas geométricos e de combinatória que podem ser 
resolvidos com o auxilio de um teorema de topologia. 

Em ambos os problemas, usaremos o conceito de \emph{posição geral}. Um conjunto \mymarginpar{posição geral}
de pontos $A\subset \RR^d$ está em posição geral se não contém $d+1$ pontos em 
um mesmo hiperplano (em particular, se $|A|>d$, então $A$ não contém 
três pontos colineares, nem quatro coplanares, etc).


\subsection{Dois problemas}

\begin{enumerate}[(1)]
 \item \textbf{Partições arco-íris}.
 
Seja $A\subset \RR^d$ um conjunto de $nd$ pontos em posição geral. Suponha que 
esses pontos estão coloridos com $n$ cores distintas e que cada cor 
aparece $n$ vezes. Em outras palavras, suponha que particionamos $A$ em conjuntos 
$A_1,\dots,A_d$, dois a dois disjuntos, com $|A_i|=n$ para todo $i$. 

\begin{theorem}[Akiyama \& Alon '89]\label{th:arcoiris}
    Nas condições acima, $A$ admite uma partição \emph{arco-íris}, isto é, \mymarginpar{partição arco-íris}
    uma partição $\{V_1, \dots, V_n\}$ tal que
    \begin{enumerate}[i)]
        \item $|V_j| = d$, para todo $j$;
        \item $\conv(V_i)\cap \conv(V_j) = \emptyset$;
        \item $|A_i\cap V_j|=1$.
    \end{enumerate}
\end{theorem}

\begin{figure}[h!]
\centering
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=.5\textwidth]{fig/08_18_fig1.png}
\end{minipage}\hfill
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=.5\textwidth]{fig/08_18_fig2.png}
\end{minipage}
\caption{\label{fig:081812} Um exemplo com $d = 2$ e $n = 4$.}
\end{figure}

\item \textbf{Colares}.

Considere um colar aberto com $d$ tipos de pedras, sendo que há um 
número par de pedras de cada tipo. Dois ladrões querem dividir o colar em 
duas partes justas (com mesmo número de pedras de cada tipo em cada 
parte) minimizando o número de cortes no colar.

\vspace{1cm}

\begin{figure}[h!]
\centering
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=.6\textwidth]{fig/08_18_fig3.png}
\end{minipage}\hfill
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=.6\textwidth]{fig/08_18_fig4.png}
\end{minipage}
\caption{\label{fig:081813} Dois exemplos. À direita, vemos que se as pedras 
de mesmo tipo estiverem agrupadas, $d$ cortes são necessários.}
\end{figure}

\begin{theorem}[T. Goldberg \& West '85]\label{th:colares}
    Se um colar aberto tem $d$ tipos de pedra, então 
    $d$ cortes são suficientes para dividir o colar em duas partes justas. 
\end{theorem}

Deixamos como exercício para o leitor encontrar uma prova puramente 
combinatória para essa proposição no caso $d = 2$.

É possível generalizar a proposição para $l$ ladrões, alterando o número de 
cortes e o requerimento de que existem um número par de pedras de cada tipo 
para um múltiplo de $l$.

\end{enumerate}
Vejamos agora um teorema de topologia usado na resolução destes dois problemas.

\subsection{\emph{The Ham Sandwich Theorem}}

A versão informal, que dá nome ao teorema, é a seguinte.
Suponha que você tem um sanduíche de presunto, formado por duas fatias de pão e 
uma fatia de presunto. Independente de como esteja montado o sanduíche, é 
possível dividi-lo em duas partes iguais (a mesma quantidade de cada fatia de 
pão e de presunto em cada parte) com apenas um corte.

Mais formalmente, sejam $\mu_1, ..., \mu_d\ d$ medidas finitas sobre $\RR^d$ 
(isto é, $\mu_i(\RR^d) < \infty$ para todo $i$), com todo aberto de $\RR^d$ 
$\mu_i$-mensurável.

Como exemplo de medida finita, considere um compacto $A \subset \RR^d$ e faça 
$\mu_A(X) = \lambda^{(d)}(X \cap A)$ para todo $X$ boleriano, onde 
$\lambda^{(d)}$ é a medida de Lebesgue usual.

\begin{theorem}[\emph{Ham Sandwich Theorem}]\label{th:HST}
 Sejam $\mu_1, ..., \mu_d$ medidas finitas sobre $\RR^d$, com todo aberto de 
$\RR^d$ $\mu_i$-mensurável, tais que para qualquer hiperplano $H \subset \RR^d$ 
temos $\mu_i(H) = 0,\ \forall i$. Então existe um hiperplano $h$ tal que 
$\mu_i(h^+) = \frac{1}{2}\mu_i(\RR^d),\ \forall i$, onde $h^+$ é um dos 
semi-espaço fechados definidos por $h$.
\end{theorem}

\begin{proof}
Seja $\uu = (u_0, ..., u_d) \in S^d = \{ (x_0, ..., x_d) \in 
\RR^{d+1}\ |\ x_0^2 + ... + x_d^2 = 1\}$. Também podemos escrever 
$\uu$ como $\uu = (u_0,w), w \in \RR^d, w = (u_1, ..., u_d)$. 

Se $|u_0| \neq 1$, defina $h^+(\uu) =\{ x \in \RR^d\ |\ \<x, 
w\> \leq u_0 \}$. Ademais, pomos $h^+(1,0,...,0) = \RR^d$ e $h^+(-1, 0, 
..., 0) = \emptyset$.

Para entender a função $h^+$, consideremos inicialmente $u_0 = 0$. Temos $w = 
v \in S^{d-1}$ e neste caso, $h^+(0,v)$ é um semi-espaço que passa pela origem 
(veja a figura \ref{fig:081815}). 

Se $u_0 \neq 0$, $\uu = (u_0, w)$, com $w = \alpha v$, $ v \in 
S^{d-1}$ e $|\alpha| < 1$. Temos $u_0^2 + \|w\|^2 = 1 \Rightarrow u_ 0^2 + 
\alpha^2 = 1$ e $\<x,w\> \leq u_0 \Leftrightarrow \<x,v\> 
\leq \frac{u_0}{\sqrt{1 - u_0^2}}$. Assim, vemos que $h^+(\uu)$ é um 
semi-espaço deslocado na direção de $v$, se $u_0 > 0$ e na direção oposta, caso 
contrário (veja a figura \ref{fig:081816}). Note que 
$h^+(\uu) \rightarrow \RR^d$, quando $u_0 \rightarrow 1$ e 
$h^+(\uu) \rightarrow \emptyset$, quando $u_0 \rightarrow -1$, o que 
justifica a definição de $h^+$ nestes casos. Aproveitamos para observar que 
$h^+(\uu)$ e $h^+(-\uu)$ são semi-espaço opostos.

\begin{figure}[h!]
\centering
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=.5\textwidth]{fig/08_18_fig5.png}
\caption{\label{fig:081815}}
\end{minipage}\hfill
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=.7\textwidth]{fig/08_18_fig6.png}
\caption{\label{fig:081816}}
\end{minipage}
\end{figure}
Seja $f : S^d \rightarrow \RR^d$, com $f = (f_1, ..., f_d)$ e $f_i = 
\mu_i(h^+(\uu))$, $\forall i$.
\begin{claim}\label{af:continua}
 f é contínua.
\end{claim}
\begin{proof}
Provaremos que para todo $i$, $\mu_i(h^+(\uu))$ é contínua em $\uu$. Suponha que 
$\uu_n \rightarrow \uu$, vamos provar que $\mu_i(h^+(\uu_n)) \rightarrow 
\mu_i(h^+(\uu))$. Para isso, escrevemos $\mu_i(h^+(\uu_n)) = \int 
\One_{h^+(\uu_n)} d\mu_i$ e $\mu_i(h^+(\uu)) = \int \One_{h^+(\uu)} d\mu_i$.
Como $|\One_{h^+(\uu_n)}| \leq 1\ \forall n$ e $\forall x \in \RR^d \backslash 
h^+(\uu),\ \One_{h^+(\uu_n)}(x) \rightarrow_n \One_{h^+(\uu)}(x)$, temos que 
$\One_{h^+(\uu_n)} \rightarrow \One_{h^+(\uu)}$ quase certamente e pelo teorema 
da convergência dominada, $\int \One_{h^+(\uu_n)} d\mu_i \rightarrow \int 
\One_{h^+(\uu)} d\mu_i$. Logo, $\mu_i(h^+(\uu_n)) \rightarrow \mu_i(h^+(\uu))\ 
\forall i$ e $f$ é contínua.
\end{proof}

Pelo teorema de Borsuk-Ulam\footnote{O teorema de Borsuk-Ulam foi tema de uma 
apresentação do PICME no semestre passado, seção 6.3 das notas de aula 
disponíveis em: 
\url{
http://www.ime.usp.br/~tcco/picme/wp-content/uploads/2015/08/PICME_2015_1.pdf}.}
, existe $\uu \in S^d$ tal que $f(\uu) = f(-\uu)$. Assim, $f_i(\uu) = f_i(-\uu)\ 
\forall i \Leftrightarrow \mu_i(h^+(\uu)) = \mu_i(h^+(-\uu))\ \forall i$.

Como $h^+(-\uu)$ é o semi-espaço oposto de $h^+(\uu)$ e sua intersecção é um 
hiperplano, por hipótese com medida nula em $\mu_i$, para todo $i$, segue que 
$\mu_i(h^+(\uu)) = \frac{1}{2}\mu_i(\RR^d)$.

\end{proof}

Para a resolução dos problemas apresentados no início dessa seção, precisamos 
de uma versão discreta do Teorema \ref{th:HST}. Para enunciá-la, 
denotaremos os semi-espaço abertos definidos por um hiperplano 
$h$ por $h^{++}$ e $h^{--}$. \mymarginpar{$h^{++}, h^{--}$}
Mais especificamente, se~$h=(a,b)$, com 
$a\in \RR^d$ e $b\in \RR$, definimos
\begin{align*}
    h^{++} = \{x\in\RR^d | \<a,x\> > b\} \quad e \quad h^{--} = \{x\in\RR^d | \<a,x\> < b\}.
\end{align*}

\begin{theorem}[\emph{Ham Sandwich} discreto]\label{th:hamsandwich}
Sejam $A_1, ..., A_d \subset \RR^d$ conjuntos finitos de pontos em $\RR^d$ com:
 
 \begin{enumerate}
  \item $A_i \cap A_ j = \emptyset$ para todo  $i\neq j$;
  \item $\bigcup_{i=1}^d A_i$ em posição geral.
 \end{enumerate}

Então existe um hiperplano $h$ em $\RR^d$ tal que cada $A_i$ é
\emph{justamente biparticionado}\mymarginpar{justamente biparticionado}
por~$h$, isto é, cada um dos semi-espaço abertos $h^++$ e $h^{--}$
contém exatamente $\lfloor\frac{|A_i|}2\rfloor$ pontos
(observe que $|h\cap A_i|=0$ se $A_i$ for par e $|h\cap A_i|=1$ se
$A_i$ for ímpar.)
\end{theorem}

\begin{proof}
Suponha inicialmente $|A_i|$ ímpar para todo $i$. Para cada $A_i$, considere 
$A_i^\epsilon = \{x \in \RR^d\ |\ d(x,A_i) \leq \epsilon\}$, com $\epsilon$ 
pequeno o suficiente para $A_i^\epsilon$ ser uma união de bolas disjuntas de 
raio $\epsilon$.

Definida a medida $\mu_i$, fazendo $\mu_i(X) = \lambda^{(d)}(X \cap 
A_i^\epsilon)$. Usando o teorema \ref{th:HST}, obtemos um hiperplano $h$ que 
divide igualmente esses conjuntos.

Fixado $A_i$, $\mu_i(h^+(A_i^\epsilon)) = \frac{1}{2} \mu_i(A_i^\epsilon)$. 
Segue que $h \cap A_i^\epsilon \neq \emptyset$ (caso contrário, teríamos 
$\left\lfloor \frac{|A_i|}{2} \right\rfloor + 1$ bolas de um dos lados de $h$) 
e $h$ intersecta alguma bola de $A_i^\epsilon$. Variando $i$, usando a 
hipótese de que os pontos estão em posição geral e fazendo $\epsilon 
\rightarrow 0$, obtemos que $h$ deve intersectar exatamente $d$ pontos, um de 
cada $A_i$ e particioná-los justamente.

Se $|A_i|$ é par, fixe $a \in A_i$ e considere $A_i \backslash \{a\}$. Pode-se 
mostrar que uma pequena pertubação de $h$ produz o resultado desejado.
\end{proof}

