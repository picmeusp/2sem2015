\begin{remark}
    Apesar da prova do Teorema \ref{th:hamsandwich} não ser construtiva, não 
    é difícil construir algoritimicamente um tal hiperplano $h$. 
    Suponha que $|A_i|$ seja ímpar para todo $i$. Então existem apenas $\prod_i |A_i|$ 
    hiperplanos candidatos a satisfazerem as condições do Teorema, a saber, aqueles 
    que intersectam com cada $A_i$ em exatamente um ponto. No caso em que alguns 
    $A_i$ têm cardinalidade par, basta adicionar pontos artificais de forma a 
    cair no caso anterior e perturbar o hiperplano obtido 
    de forma a biparticionar justamente as coleções originais (sem pontos extras).
\end{remark}

    
\begin{proof}[Prova do Teorema \ref{th:arcoiris}]
    Por indução em $n$. Para~$n=0$ não há nada a ser provado. Para~$n>0$, aplique
    o Teorema~\ref{th:hamsandwich} e observe que ambas as coleções de pontos 
    contidas em~$h^{++}$ e~$h^{--}$ satisfazem as condições do teorema e, portanto, 
    possuem partições arco-íris por hipótese de indução. Se~$n$ for par, a união dessas
    duas partições já é uma partição arco-íris de~$A$. Se~$n$ for ímpar, basta também 
    considerar a parte formada pelos pontos contidos em~$h$. 
\end{proof}

\begin{proof}[Prova (Alon) do Teorema \ref{th:colares}]
No caso em que $d=2$, podemos associar as pedras a pontos em um círculo e
aplicar o Teorema \ref{th:hamsandwich} para obter a bipartição desejada. 

No caso geral, usaremos a \emph{curva dos momentos} de dimensão $d$, \mymarginpar{curva dos momentos}
dada pela equação paramétrica
$$\gamma(t) = (t, t^2, \dots, t^d)\in \RR^d,\quad t\geq 0.$$
As seguintes propriedades da curva $\gamma$ serão suficientes para provarmos
o teorema. 
\begin{enumerate}
    \item {\sl Se $t_0, \dots, t_d \geq 0$ são todos distintos, então 
    $\gamma(t_0), \dots, \gamma(t_d)$ não estão em um mesmo hiperplano.}
        \begin{proof}
        Suponha o contrário e seja $a=(a_1,\dots,a_d)\not=0\in \RR^d$ e 
        $b\in \RR$ tais
        que 
        $$\<a, \gamma(t_i)\> = b, \mathrlap{\qquad\qquad (i=0,\dots,d)}$$
        ou seja,
        $$
        \begin{matrix}
            a_1t_0 &+ &\dots &+ &a_dt_0^d &= &b \\
            a_1t_1 &+ &\dots &+ &a_dt_1^d &= &b \\
            \vdots &  &  & &\vdots   &=  &\vdots\\
            a_1t_d &+ &\dots &+ &a_dt_d^d &= &b \\
        \end{matrix}$$

        Defina o polinômio $P(x) = -b + a_1x + \dots + a_dx^d$. 
        Vemos que~$P(x)$ tem~$d+1$ raízes distintas, da onde segue 
        que~$P(x) = 0$, o que contradiz a hipótese de que~$a\not=0$.
        \end{proof} 
    \item {\sl Todo hiperplano em $\RR^d$ encontra a curva $\gamma$ em no
    máximo $d$ pontos. }
        \begin{proof}
            Segue da propriedade anterior.
        \end{proof}
\end{enumerate}

Assim, se um colar tem $n$ pedras, basta associá-las (na ordem em que
aparecem no colar) a pontos $\gamma(1), \dots, \gamma(n)$ e aplicar o
Teorema \ref{th:hamsandwich}.
\end{proof}

A seguir, vamos mostrar uma variante do Teorema \ref{th:hamsandwich} em que
descartamos a hipótese dos pontos estarem em posição geral. Nesse caso, o
exemplo da figura \ref{fig:hamcontraexemplo} mostra que infelizmente não podemos garantir a
existência de um hiperplano que biparticione \emph{justamente} cada $A_i$. 
\begin{figure}[h]\centering 
\begin{minipage}{0.8\textwidth}
\centering
\begin{tikzpicture}[scale=2.2]
    \draw[dashed] (-2.7,0)--(2.7,0);
    \draw[fill=gray] (-2.5, 0) circle(3pt);
    \draw[fill=gray] (-1.5, 0) circle(3pt);
    \draw[fill=gray] (-2,0.866) circle(3pt);
    \draw[fill=blue] (2.5,0) circle (3pt);
    \draw[fill=blue] (1.5, 0) circle (3pt);
    \draw[fill=blue] (2,0.866) circle(3pt);
\end{tikzpicture}
\caption{ \small Não existe hiperplano que biparticiona justamente as duas coleções de pontos acima}
\label{fig:hamcontraexemplo}
\end{minipage}
\end{figure}

Seja $A\subset \RR^d$ um conjunto finito de pontos. Dizemos que um hiperplano 
$h$ \emph{biparticiona fracamente} $A$ se \mymarginpar{biparticiona fracamente}
$|h^{++}\cap A|,|h^{--}\cap A|\leq|A|/2$.

\begin{theorem}
    Sejam $A_1,\dots, A_d\subset \RR^d$, com cada $A_i$ finito. Então existe 
    um hiperplano $h$ que biparticiona fracamente cada um dos $A_i$. 
\end{theorem}
\begin{proof}
    Considere $A_i^{(\eta)}$ uma $\eta$-perturbação de $A_i$. Podemos supor que
    $\cup_i A_i^{(\eta)}$ está em posição geral.  Aplique o Teorema
    \ref{th:hamsandwich} para obter um hiperplano $h^{(\eta)}$ dado 
    por $(a^{(\eta)}, b^{(\eta)})$. Faça $\eta\to 0$. Como 
    $\|a^{(\eta)}\|=1$ e $|b^{(\eta)}|$ é limitado, podemos supor 
    que~${a^{(\eta)}\to a}$ e~$b^{(\eta)}\to b$. O hiperplano 
    $h = (a,b)$ satisfaz as condições desejadas.
\end{proof}

\subsection{Conjectura de Kneser}

O seguinte resultado foi conjecturado por Kneser ('55) e 
demonstrado por Lovász ('78). A demonstração de Lovász faz uso do Teorema 
de Borsuk-Ulam e será apresentada na seção \ref{sec:kneser}.

\begin{theorem}[Conjectura de Kneser/Teorema de Lovász]\label{th:kneserlovasz}
    Seja $n>2k-1$ e suponha que existam conjuntos
      $\cC_1, \dots, \cC_{n-2k+1} \in 2^{\binom{[n]}{k}}$ tais que 
    $$\binom{[n]}{k} = \cC_1 \mathbin{\dot\cup} \dots \mathbin{\dot\cup} \cC_{n-2k+1}.$$
    Então existe $i$ e conjuntos $A,B\in \cC_i$ tais que $A\cap B=\emptyset$.
\end{theorem}
\begin{remark}
    O resultado acima é falsa para o caso em que $\binom{[n]}{k}$ 
    é particionado em $n-2k+2$ partes. Uma 3-coloração do grafo 
    de Petersen é um contraexemplo para o caso em que $n=5$ e $k=2$.

\end{remark}
    
    
