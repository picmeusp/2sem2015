A \emph{cintura} $g(G)$ de um grafo $G$ é o tamanho do menor 
ciclo do grafo. Também definimos o \emph{grau mínimo} $\delta(G)$
de $G$ como o menor grau de um vértice em $G$.

Dados inteiros $r$ e $g$, estamos interessados em determinar
 o menor número de vértices que um grafo $G$ de grau mínimo 
 $r$ e cintura $g$ pode ter. Denotaremos esse número por 
 $n(r,g)$. Mais formalmente, podemos definir 
$$n(r,g) = \min\{|V(G)|: \delta(G)=r \text{ e } g(G)=g\}.$$

É fácil verificar, por exemplo, que 
\begin{itemize}
    \item $n(2,5)=5$ (o ciclo de tamanho $5$ atinge o mínimo),
    \item $n(3,3)=4$ (o grafo completo $K^4$ atinge o mínimo) e 
    \item $n(3,4)=6$ (o grafo bipartido $K_{3,3}$ atinge o mínimo).
\end{itemize}

\begin{proposition}
    Se $g=2k+1$, então 
    $$n(r,g) \geq 1 + r + r(r-1) + \dots +r(r-1)^{k-1}.$$
\end{proposition}
\begin{proof}
    Seja $G$ um grafo tal que $\delta(G)=r$ e $g(G)=g$. Considere
    uma árvore de busca em largura a partir de um vértice arbitrário $v$
    de $G$. Como $\delta(G)=r$, o primeiro nível tem pelo menos 
    $r$ vértices e, para $i\geq 1$, o $i$-ésimo nível tem 
    pelo menos $r(r-1)^{i-1}$ vértices. Ademais, os vértices 
    que aparecem até o nível $k$ são todos distintos, caso 
    contrário haveria dois caminhos distintos de tamanho no máximo $k$ 
    de um mesmo vértice $u$ até $v$, ou seja, um ciclo de tamanho 
    menor que $2k+1$.
\end{proof}

\begin{proposition}
    Se $g=2k$, então 
    $$n(r,g) \geq 1 + r + r(r-1) + \dots + r(r-1)^{k-2} + (r-1)^{k-1}.$$
\end{proposition}
\begin{proof}
    Seja $G$ um grafo tal que $\delta(G)=r$ e $g(G)=g$. Procedemos 
    como no caso anterior, considerando uma árvore em busca em largura
    a partir de um vértice arbitrário $v$. Temos que levar em consideração que 
    um vértice $u$ no nível $k$ pode aparecer múltiplas vezes. Note, contudo, 
    que $u$ não pode ser adjacente a mais do que $r$ vértices do nível $k-1$
    (caso contrário haveria caminhos distintos de tamanho $k-1$ de $u$ a um
    vizinho de $v$). Portanto há pelo menos $(r-1)^{k-1}$ vértices distintos no 
    $k$-ésimo nível.
\end{proof}
\begin{exercise}
    Provar a proposição anterior, considerando uma árvore de busca em largura 
    a partir de um vértice artificial $v\notin V(G)$ adjacente a dois vértices 
    $v_1,v_2\in V(G)$ com $v_1v_2\in E(G)$ (note que a cota inferior da 
    proposição é igual a $2\sum_{i=0}^{k-1} (r-1)^i$).
\end{exercise}


Nesta seção, estamos interessados em demonstrar o seguinte resultado.
\begin{theorem} 
    Seja $r\geq 3$ e suponha que exista um grafo $G$ 
    de tamanho~$n=1+r+r(r-1)=r^2+1$, 
    cintura~$g(G)=5$ e 
    grau mínimo~$\delta(G) = r$. Então $r=3,5$ ou $57$.
\end{theorem}

Dizemos que $\lambda\in\RR$ é um \emph{autovalor} de uma matriz~$A\in
\RR^{n\times n}$ se existe~$x\in \RR^n$, $x\not=0$, 
tal que~$Ax = \lambda x$. Nesse caso, dizemos que $x$ é um 
\emph{autovetor} associado a $\lambda$. O \emph{autoespaço}
associado a $\lambda$ é o conjunto de todos os autovetores 
associados a $\lambda$. 

\begin{exercise}
    Mostrar que o autoespaço associado a um autovalor $\lambda$ é, 
    de fato, um espaço linear.    
\end{exercise}

\begin{lemma} Toda matriz simétrica $A\in\RR^{n\times n}$ 
possui~$n$ autovetores dois a dois ortogonais.
\end{lemma}
\begin{proof}
    Primeiro note que quaisquer $x,y\in\RR^{n}$ satisfazem
    $\<Ax,y\> = \<x,Ay\>$. De fato, 
    $$\<Ax,y\> = (Ax)^Ty = x^TA^Ty = x^TAy = \<x,Ay\>.$$

    Sejam $v_1,v_2$ autovetores de $A$ associados a autovalores 
    distintos $\lambda$ e $\mu$, respectivamente. Então
    $$\lambda\<v_1,v_2\> = \<\lambda v_1,v_2\> = \<Av_1,v_2\> = 
    \<v_1, Av_2\> = \<v_1, \mu v_2\> = \mu\<v_1,v_2\>.$$
    Logo, $(\lambda-\mu)\<v_1,v_2\> = 0$, da onde segue que 
    $\<v_1,v_2\> = 0$.

    Considere $\{u_1,\dots,u_{\ell}\}$ uma base do autoespaço associado a
    um autovalor $\lambda$. Usando o processo de ortogonalização 
    de Grahn-Schimidt, conseguimos uma base ortogonal $\{u_1', \dots, u_{\ell}'\}$
    exercicio do autoespaço associado a $\lambda$.

    {\color{red} Talvez faltaria falar que a soma das dimensões desses 
    autoespaços é $n$?}
\end{proof}


