
\subsection{Sistemas de Massas}

Consideraremos sistemas de pontos no plano nos quais valores de \emph{massa} 
são associados a cada ponto.

\begin{notation}
    Um ponto $(x,y)\in\RR^2$ no plano associado a uma massa 
    $m\in \RR$ será denotado por $(x,y)[m]$. 
\end{notation}

O \emph{centro de massa} de um sistema de pontos 
$\{(x_i,y_i)[m_i]\}_{i=1}^n$ é o ponto~$(x_c,y_c)[m]$, onde 
\begin{align*}
    m = \sum_{i=1}^n m_i,
    &&
    x_c = \frac{\sum_{i=1}^n x_i}{m}, 
    &&
    y_c = \frac{\sum_{i=1}^n y_i}{m}.
\end{align*}
\begin{proposition}\label{prop:massa2}
    O centro de massa $G[m+n]$ de dois pontos~$A[m]$ e~$B[n]$ é tal que 
    $A,B$ e $G$ são colineares e, além disso,
    $$\overline{AG}\cdot m = \overline{GB}\cdot n.$$\qed
\end{proposition}

\begin{proposition}\label{prop:ordem}
    Sejam~$(x_c, y_c)[m]$ o centro de massa de um sistema~$S = \{(x_i,y_i)[m_i]\}_{i=1}^n$ 
    e~$(x'_c, y'_c)[m']$ o centro de massa 
    de um sistema~$S' = \{(x'_i, y'_i)[m'_i]\}$. Então o centro de massa 
    de $S\cup S'$ é o centro de massa de $\{(x_c,y_c)[m], (x'_c, y'_c)[m']\}$.\qed
\end{proposition}

A seguir consideraremos triângulos $ABC$ quaisquer e denotaremos por~$a,b,c$ os
comprimentos dos lados opostos aos vértices $A,B,C$ respectivamente.

Uma \emph{seviana} é qualquer segmento de reta que une um vértice a um ponto do lado oposto.
Em particular, a \emph{mediana} é uma seviana que une que um vértice ao ponto médio do lado oposto.
O \emph{baricentro} de um triânglo é o ponto de encontro de suas três
medianas. 

\begin{lemma}\label{lem:baricentro}
    O baricentro de um triângulo $ABC$ é dado pelo centro de massa 
    de $A[p]$, $B[p]$ e $C[p]$, onde $p$ é um valor arbitrário de massa.
\end{lemma}
\begin{proof}
    Seja $G[3p]$ o centro de massa de $A[p], B[p]$ e $C[p]$ e 
    seja $Q,R$ e $T$ os pontos que definem as três medianas, como abaixo.

    {\hfill \begin{tikzpicture}[scale=1.25]%,cap=round,>=latex]
    \coordinate [label=left:$B$] (B) at (-2,0);
    \coordinate [label=right:$C$] (C) at (4, 0);
    \coordinate [label=above:$A$] (A) at (0,3);
    \coordinate (Q) at (1,0);
    \coordinate (R) at (2,1.5);
    \coordinate (T) at (-1,1.5);
    \coordinate (G) at (2/3, 1);
    \draw (Q) circle (1pt) node[anchor=north]{$Q$};
    \draw (R) circle (1pt) node[anchor=south]{$R$};
    \draw (T) circle (1pt) node[anchor=south]{$T$};
   % \draw (M) circle (1pt) node[anchor=north]{$M$};
    %\draw (A) -- node[left] {$c$} (B) -- node[below] {$a$} (C) -- node[above] {$b$} (A);
    \draw[line width=1.5] (A) -- (B) -- (C) -- (A);
    \draw[dashed] (A)-- (Q);
    \draw[dashed] (B)-- (R);
    \draw[dashed] (C)-- (T);
    \tkzMarkSegment[color=gray,pos=.5,mark=|](B,Q);
    \tkzMarkSegment[color=gray,pos=.5,mark=|](Q,C);
    \tkzMarkSegment[color=gray,pos=.5,mark=||](B,T);
    \tkzMarkSegment[color=gray,pos=.5,mark=||](T,A);
    \tkzMarkSegment[color=gray,pos=.5,mark=|||](A,R);
    \tkzMarkSegment[color=gray,pos=.5,mark=|||](R,C);

    %\draw [decoration={ brace, mirror, raise=15 }, decorate]
    %     (B) -- (C) node[pos=0.5, anchor=north, yshift=-16]{$a$};
    %\draw [decoration={ brace, mirror, raise=15 }, decorate]
    %     (A) -- (B) node[pos=0.5, anchor=north, yshift=16]{$c$};
    \end{tikzpicture}\hfill}

    Pela Proposição \ref{prop:massa2}, o ponto $Q[2p]$ é o centro de 
    massa de $B[p]$ e $C[p]$. Pela Proposição $\ref{prop:ordem}$, 
    $G[3p]$ é, também, o centro de massa de $A[p]$ e $Q[2p]$, da onde
    segue que $G$ está sobre a mediana $AQ$. De maneira
    simétrica, é possível concluir que $M$ também está sobre as demais 
    medianas e, portanto, que $G$ é o baricentro de $ABC$.
\end{proof}

O \emph{incentro} de um triângulo é o ponto de encontro das sevianas que 
bissectam cada ângulo.

\begin{lemma}\label{lem:incentro}
    O incentro de um triângulo $ABC$ é dado pelo centro de massa 
    dos pontos $A[a]$, $B[b]$ e $C[c]$. 
\end{lemma}
\begin{proof}
    Seja $I[a+b+c]$ o centro de massa de $A[a]$, $B[b]$ e $C[c]$ e sejam 
    $Q, R$ e $S$ pontos que definem cada uma das bissetrizes de $ABC$ (ver figura abaixo).

    {\hfill \begin{tikzpicture}[scale=1.25]%,cap=round,>=latex]
    \coordinate [label=left:$B$] (B) at (-2,0);
    \coordinate [label=right:$C$] (C) at (4, 0);
    \coordinate [label=above:$A$] (A) at (0,3);
    \coordinate (Q) at (0.833,0);
    \coordinate (R) at (1.71, 1.72);
    \coordinate (T) at (-0.909,1.636 );
    \coordinate (M) at (2/3, 1);
    \draw (Q) circle (1pt) node[anchor=north]{$Q$};
    \draw (R) circle (1pt) node[anchor=south]{$R$};
    \draw (T) circle (1pt) node[anchor=south]{$T$};
   % \draw (M) circle (1pt) node[anchor=north]{$M$};
    %\draw (A) -- node[left] {$c$} (B) -- node[below] {$a$} (C) -- node[above] {$b$} (A);
    \draw[line width=1.5] (A) -- (B) -- (C) -- (A);
    \draw[dashed] (A)-- (Q);
    \draw[dashed] (B)-- (R);
    \draw[dashed] (C)-- (T);
    \tkzMarkAngle[color=gray,mark=|,size=0.5](B,A,Q);
    \tkzMarkAngle[color=gray,mark=|,size=.6](Q,A,C);
    \tkzMarkAngle[color=gray,mark=||,size=0.6](C,B,R);
    \tkzMarkAngle[color=gray,mark=||,size=.7](R,B,A);
    \tkzMarkAngle[color=gray,mark=|||,size=0.8](A,C,T);
    \tkzMarkAngle[color=gray,mark=|||,size=.9](T,C,B);
    \tkzMarkAngle[fill=blue,opacity=.4,size=.4](A,Q,B);
    \tkzLabelAngle[color=blue,pos=.5](A,Q,B){\footnotesize$\beta$};
    

    \draw [color=gray,decoration={ brace, mirror, raise=2 }, decorate]
         (B) -- (Q) node[pos=0.5, anchor=north, yshift=-3]{$m$};
    \draw [color=gray,decoration={ brace, mirror, raise=2 }, decorate]
         (Q) -- (C) node[pos=0.5, anchor=north, yshift=-3]{$n$};
    \draw [color=gray,decoration={ brace, mirror, raise=15 }, decorate]
         (A) -- (B) node[pos=0.5, anchor=south, yshift=5,xshift=-20]{$c$};
    \draw [color=gray,decoration={ brace, mirror, raise=15 }, decorate]
         (C) -- (A) node[pos=0.5, anchor=south, yshift=10, xshift=15]{$b$};
    \end{tikzpicture}\hfill}

    Se aplicarmos a \emph{Lei dos Senos} aos triângulos $ABQ$ e $AQC$, obtemos, 
    respectivamente, que
    \begin{align*}
        \frac{m}{\sin \alpha} = \frac{c}{\sin\beta}
        &&\text{e}&&
        \frac{n}{\sin\alpha} = \frac{b}{\sin(\pi-\beta)} = \frac{b}{\sin\beta}.
    \end{align*}
    Daí concluímos que $mb = nc$. Pela Proposição \ref{prop:massa2}, 
    o ponto $Q[b+c]$ é o centro de massa de $B[b]$ e $C[c]$. Logo $I$ é 
    também o centro de massa $A[a]$ e $Q[b+c]$ e, portanto, está 
    contido na bissetriz $AQ$. Por simetria, concluímos que 
    $I$ também está sobre as demais bissetrizes e, é, portanto, 
    o incentro de $ABC$.
\end{proof}

%TODO TODO TODO TODO
{\it\bf(Definição ponto Nagel)}

    {\hfill \begin{tikzpicture}[scale=0.8]%,cap=round,>=latex]
    \clip(-5,4) -- (7,4) -- (7,-7.5) -- (-5,-7.5)  -- (-5,4);
%    \tkzInit[xmax=6,ymax=4,xmin=-5,ymin=-7] \tkzGrid
    \coordinate [label=left:$B$] (B) at (-2,0);
    \coordinate [label=right:$C$] (C) at (4, 0);
    \coordinate [label=above:$A$] (A) at (0,3);
    \tkzDefPointWith[linear normed, K=3.697](B,C); \tkzGetPoint{Q};
    \tkzDefPointWith[linear normed, K=3.697](A,C); \tkzGetPoint{R};
    \tkzDefPointWith[linear normed, K=2.303](A,B); \tkzGetPoint{T};
    \tkzDefPointWith[linear normed, K=7.303](A,B); \tkzGetPoint{E};
    \tkzDefPointWith[linear normed, K=7.303](A,C); \tkzGetPoint{F};
    \tkzCircumCenter(E,Q,F)\tkzGetPoint{O};
    \tkzDrawArc(O,F)(E);
    \tkzDrawPoints[color=red](Q,R,T);
    \tkzDrawPoints[color=green](O,E,F);
    \tkzDrawPoints[color=black](A,B,C);
    \tkzLabelPoints(E,F,O)
    \tkzMarkRightAngle(B,E,O);
    \tkzMarkRightAngle(C,F,O);

    \draw (Q)  node[anchor=north]{$Q$};
    \draw (R)  node[anchor=south]{$R$};
    \draw (T)  node[anchor=south]{$T$};


   % \draw (M) circle (1pt) node[anchor=north]{$M$};
    %\draw (A) -- node[left] {$c$} (B) -- node[below] {$a$} (C) -- node[above] {$b$} (A);
    \draw[line width=1.2] (A) -- (B) -- (C) -- (A);
    \draw[dotted] (B)--(E);
    \draw[dotted] (C)--(F);
    \draw[dotted] (F)--(O);
    \draw[dotted] (E)--(O);
    %\draw (1.68,-6.83) arc (30:150:6.8cm);
    \draw[dashed] (A)-- (Q);
    \draw[dashed] (B)-- (R);
    \draw[dashed] (C)-- (T);
    

    \draw [color=gray,decoration={ brace, mirror, raise=2 }, decorate]
         (B) -- (Q) node[pos=0.5, anchor=north, yshift=-3]{$m$};
    \draw [color=gray,decoration={ brace, mirror, raise=2 }, decorate]
         (Q) -- (C) node[pos=0.5, anchor=north, yshift=-3]{$n$};
    \draw [color=gray,decoration={ brace, mirror, raise=15 }, decorate]
         (A) -- (B) node[pos=0.5, anchor=south, yshift=5,xshift=-20]{$c$};
    \draw [color=gray,decoration={ brace, mirror, raise=15 }, decorate]
         (C) -- (A) node[pos=0.5, anchor=south, yshift=10, xshift=15]{$b$};
    \end{tikzpicture}\hfill}



\begin{lemma}\label{lem:nagel}
    O ponto de Nagel do triângulo $ABC$ acima é  dado pelo centro de massa 
    dos pontos $A[p-a]$, $B[p-b]$ e $C[p-c]$, onde $p$ é o semiperímetro
    de $ABC$. 
\end{lemma}
\begin{proof}
    Seja $N$ o centro de massa definido como acima e $\overline{AQ}$, 
    $\overline{BR}$ e $\overline{CT}$ as sevianas que definem o ponto 
    de Nagel. 

    Como  $\overline{BE} = \overline{BD} = m$ e $\overline{CD} = \overline{CF} = n$, 
    devemos ter 
    $$c+m = \overline{AE} = \overline{AF} = b + n.$$
    Substituindo $m=a-n$, obtemos 
    $$c-a-n = b+n,$$
    isto é,
    $$n = \frac{a-b+c}{2} = p-b.$$
    De forma simular, temos $m = p-c$. Logo, $Q[a]$ é o centro de massa 
    de $B[p-b]$ e $C[p-c]$. Então $N$ também pode ser escrito como 
    o centro de massa de $Q[a]$ e $A[p-a]$, da onde segue que 
    $N$ está contido em $\overline{AQ}$. De forma análoga, é possível 
    mostrar que $N$ também está contido em $\overline{CT}$ e $\overline{BR}$.
\end{proof}


\begin{theorem} 
    Sejam $G,I,N$ o baricentro, o incentro e o ponto de Nagel (respectivamente) 
    de um triângulo $ABC$. Então $G,I,N$ são colineares e
$$\frac{\overline{NG}}{\overline{GI}} = \frac21.$$ 
\end{theorem}
\begin{proof}
    Pelo Lema~\ref{lem:incentro}, sabemos que~$I[2p]$ é o centro de massa 
    do sistema~$\{A[a],B[b],C[c]\}$ e pelo Lema~\ref{lem:nagel} que~$N[p]$ é o centro de massa 
    do sistema~$\{A[p-a],B[p-b],C[p-c]\}$. Logo, o centro de massa de $I[2p]$ e $N[p]$ 
    é o centro de massa do sistema
    $$\{A[a+p-a], B[b+p-b], C[c+p-c]\} = \{A[p], B[p], C[p]\}, $$
    que é o ponto $G[3p]$, pelo Lema~\ref{lem:baricentro}. Concluímos então que 
    $G,I,N$ são colineares e que 
    $$\overline{GN}\cdot p = \overline{IG}\cdot 2p,$$
    o que implica a relação desejada.
\end{proof}


\begin{theorem}[Ceva]
\end{theorem}



